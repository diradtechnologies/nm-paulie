const crypto = require("crypto"); //Used for hashing
const Polly = require('polly-tts');
const fs = require('fs')
const wav = require('wav').FileWriter;


var publicFolder;
var appLocation;
var polly;

module.exports = {
    init: function init(data) {
        publicFolder = data.publicFolder;
        appLocation = data.appLocation;
        polly = new Polly({ accessKeyId: data.accessKeyId, secretAccessKey: data.secretAccessKey });
    },
    tts: async function tts(verbiage, language, voice) {
        if (fs.existsSync(publicFolder + '/' + ((language != undefined) ? language : 'english') + '/' + crypto.createHash('md5').update(verbiage).digest("hex") + '.wav')) {
            return appLocation + '/' + ((language != undefined) ? language : 'english') + '/' + crypto.createHash('md5').update(verbiage).digest("hex") + '.wav'
        } else {
            console.log('here')
            return makewav(verbiage, language, voice)
        }
    }
}

function makewav(verbiage, language, voice) {
    return new Promise((resolve, reject) => {
        var options = {
            text: verbiage, // if textType is ssml, than here needs to be the ssml string
            textType: "text", // marks if it is ssml, text etc. - optional
            region: "us-east-1", // aws region - optional
            voiceId: (voice != undefined) ? voice : (language == 'spanish') ? "Penelope" : "Joanna", // Polly Voice -> also determines the language - optional
            sampleRate: 8000, // optional
            outputFormat: "pcm" // all polly output formats like mp3, pcm etc. - optional
        };
        // Pipe the synthesized text to a file.
        polly.textToSpeech(options, function (err, audioStream) {
            if (err) {
                reject(err)
            } else {
                var save = new wav(publicFolder + '/' + ((language != undefined) ? language : 'english') + '/' + crypto.createHash('md5').update(verbiage).digest("hex") + '.wav', {
                    sampleRate: 8000,
                    channels: 1
                });
                //console.log("Length: " + current.length);
                audioStream.pipe(save);
                resolve(appLocation + '/' + ((language != undefined) ? language : 'english') + '/' + crypto.createHash('md5').update(verbiage).digest("hex") + '.wav');
            }
        });
    })
}

