# Paulie

Discount Polly Prompt Generator. Will generate hashed versions of polly prompts and store them in the public folder.

### Add to your code

This is not added as a usual npm module as it is not listed on npm. The code is stored publicly in our bitBucket. 

To add the code to your project add the folowing to your dependencies in your package.json file. 

```json
{
"paulie": "git+https://bitbucket.org/diradtechnologies/nm-paulie.git"
}
```

It can then be used as a normal module at the top of your code.

```javascript
const paulie = require('paulie')
```

## Usage

### init

Use the init function to specify your public folder and your app location

```javascript
const paulie = require('paulie')

paulie.init({
    "publicFolder": __dirname + '/public',
    "appLocation": "https://example-flow.dirad.cloud/flow",
    "accessKeyId": "wertyuiokdf184",
    "secretAccessKey": "fthgjklfpouigyhjbnkm32987yu2nj0"
})
```

### tts

tts will return a string of the location of the hashed prompt file that was either just created, or already existed in the filesystem. You can specify, your language and the voice optionally it will defualt to Joanna and English if you do not choose

```javascript

gather.play(await paulie.tts("Hey there, I'm Paulie the parrot!", payload.language));

```



